var siteScript = {
	init: function() {
		this.siteScroll();
		this.parallax();
		this.subnav();
		this.homeSlider();
		this.servicesSlider();
		this.aboutSlider();
		this.spotlight();
		this.customSelect();
		this.otherScript();
		this.eqHeight();
		this.resize();
	},

	siteScroll: function() {
		if ($.fn.impulse) {
			$(window).on('load', function() {
			  $(this).impulse();
			});
		}
	},

	parallax: function() {
		if ($.fn.parallax) {
			$(document).on('ready', function() {
				$('.parallax').parallax();
			});
		}
	},

	subnav: function() {
		$('.page-nav ul li').hover(function(){
			$(this).toggleClass("hover");
			$(this).find('.subnav').stop().slideDown();
		}, function(){
			$(this).find('.subnav').stop().slideUp();
		});

		$('.page-nav li').each(function(){
			$(this).find('.subnav').parent().addClass('sub');
		});

		$('.nav-trigger').on('click', function(){
			$('body').toggleClass('on');
		});

		$('.page-overlay').on('click', function(){
			$('body').removeClass('on');
		});

		$('.mobile-menu ul li a').on('tap', function(){
			$(this).parent('li').find('.subnav').stop().slideToggle();
		});

		$('#search').on('click', function(){
			$('.page-search').toggleClass('on');
		});

	    var toggles = document.querySelectorAll("#toggle");

	    for (var i = toggles.length - 1; i >= 0; i--) {
	      var toggle = toggles[i];
	      toggleHandler(toggle);
	    };

	    function toggleHandler(toggle) {
	      toggle.addEventListener( "click", function(e) {
	        e.preventDefault();
	        (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
	      	(this.classList.contains("is-active") === true) ? $('body').addClass('on') : $('body').removeClass('on');
	      });
	    }
	},

	homeSlider: function() {
		if ($.fn.owlCarousel) {
			$("#home-slider").owlCarousel({

				navigation: true, 
				slideSpeed: 300,
				paginationSpeed: 400,
				transitionStyle : "fade",
				singleItem: true,
				autoPlay: true

				// "singleItem:true" is a shortcut for:
				// items : 1, 
				// itemsDesktop : false,
				// itemsDesktopSmall : false,
				// itemsTablet: false,
				// itemsMobile : false

			});
		}
	},

	servicesSlider: function() {
		if ($.fn.owlCarousel) {
			$("#services-slider").owlCarousel({
				navigation: true,
				autoPlay: 5000, 
				items : 3,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3]
			});
		}
	},
	
	aboutSlider: function() {
		if ($.fn.owlCarousel) {
			$("#about-slider").owlCarousel({
				navigation: true,
				autoPlay: 5000, 
				items : 3,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3]
			});
		}
	},

	spotlight: function() {
        // var windW = $(window).width();
        // var windH = $(window).outerHeight();
        // var headerH = $('header').outerHeight();
        // var newHeight = windH-headerH;
        // var sections = $('.page-spotlight .item')
        // $(sections).css('height', windH);

		$('.home-slider .item img').each(function() {
			var imgSrc = $(this).attr('src');
			$(this).parent('.item').css({
				backgroundImage: 'url(' + imgSrc + ')'
			});
		});
    },

	spotCaption: function() {
		$(window).on('load', function(){
			// var windH = $(window).outerHeight();
			var itemH = $('.page-spotlight .item').outerHeight();
			var windW = $(window).width();
			var headerH = $('header').outerHeight();
			var newH = itemH-headerH
			$('.page-spotlight .item .elem-animated').css('bottom', -newH);
			
			var capH = $('.page-spotlight .item .caption').outerHeight();
			var capW = $('.page-spotlight .item .caption').width();
			$('.page-spotlight .item .caption').css({
				top: itemH/2-capH/2,
			});
		});
	},

	customSelect: function() {
		if ($.fn.selectBoxIt) {
			$("select").selectBoxIt();
		}
	},

	otherScript: function() {
		
	},
	
	eqHeight: function() {
		var maxHeight1 = 0;
		$('.about-slider .item .content').each(function() {
			maxHeight1 = ($(this).outerHeight() > maxHeight1) ? $(this).outerHeight() : maxHeight1;
		});
		$('.about-slider .item .content').css("height", maxHeight1);

		var maxHeight2 = 0;
		$('.our-services .service-list-holder').each(function() {
			maxHeight2 = ($(this).outerHeight() > maxHeight2) ? $(this).outerHeight() : maxHeight2;
		});
		$('.our-services .service-list-holder').css("height", maxHeight2);

	},

	resize: function() {
		$(window).on('resize', function(){
			// $('.page-spotlight .item').css("height", 'auto');
			// siteScript.spotlight();
			// siteScript.eqHeight();
		});
	}
}

$(function() {
	siteScript.init();
});